Spaces OG Login/Logout
----------------------

This module compliments more popular and better known Login Toboggan
and Login/Logout Destinations. It performs similar functions, but is
specifically targeted at sites, running Spaces and Organic Groups.

The typical use of Spaces and Organic Groups is to segregate sections
of the site and associate users with these sections. It is not unusual
to use Spaces and Organic Groups to create an appearance of multiple,
independently running sites on top of single Drupal deployment. In
doing so, site owners often encounter a problem, related to user's
login/logout experience: the standard Drupal implementation takes
users to user's profile page upon login and takes to the home page of
the overall site upon logout. This module provides a feature, which,
if enabled within given space and organic group, alters the flow as
follows:

a. The user visited a page within the organic group and he/she is not
logged in, while the page requires login. After successful login the
user is taken to the home page for the space/group.

b. The user is logged in and clicks the logout link on a page within
the organic group - after the logout she or he will be taken to the
home page for that organic group.

The net effect of these two flows is that the user is kept within the
organic group throughout his/her experience and never taken to other
organic groups or overall site home, unless there are links,
specifically pointing there.


INSTALLATION
------------

a. Download the module from drupal.org

b. Untar or unzip contents and copy to sites/all/modules directory
under your Drupal installation

c. Enable the module either through UI (Admin -> Modules -> Check the
Spaces OG Login / Logout) or using drush "drush pm-enable
spaces_og_login__logout"

d. Log in with permissions to "administer spaces": either use and
Admin account or go to People -> Permissions and check "administer
spaces" checkbox for a role, granted to the user, who will be doing
the configuration

e. Go to the Features page for the organic group where the
login/logout functionality is to be altered. Usually the Features page
is under ?q=/node/<nid>/features. nid is the node id for your organic
group

f. Choose "Enable" next to Spaces OG Login/Logout, click "Submit".

g. Click on the "Settings" link next to Spaces OG Login/Logout to
configure parameters affecting the login/logout behavior.
